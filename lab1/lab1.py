import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import random
from scipy.spatial import distance

#generacja przeszkód
def obsticle_creator(n_o):
    qoi = np.zeros([n_o, 2])
    for i in range(0, n_o):
        x1 = random.uniform(-10, 10)
        y1 = random.uniform(-10, 10)
        qoi[i] = [x1, y1]
    return qoi

#liczenie siły przyciągającej
def Fp(qr, qk, kp):
    dist = distance.euclidean(qr, qk)
    return kp*dist

#liczenie siły odpychającej przeszkody
def Fo(qr, qo, ko, d0, limit):
    dist = distance.euclidean(qr, qo)
    if dist > d0:
        return 0
    if dist == 0:
        return limit
    return ko * (1 / dist - 1 / d0) * (1 / (dist ** 2))

#sumowanie sił odpychających przeszkód
def sum_Fo(qr, tp, koi, d0, limit=5):
    t_Fp = 0
    for i in tp:
        t_Fp += Fo(qr, i, koi, d0, limit)
    return t_Fp

def main():
    flat = False
    norm = False
    n_o = 3
    x_size = 10
    y_size = 10
    delta = .1

    k0 = 1
    koi = 1
    d0 = 20

    qpr = [-10, random.randint(-10, 10)]
    qkr = [10, random.randint(-10, 10)]

    print("Start:", qpr)
    print("Finish:", qkr)

    x = y = np.arange(-10, 10, delta)
    X, Y = np.meshgrid(x, y)

    Z = np.exp(-X ** 0)

    qoi = obsticle_creator(n_o)

    i = 0

    #sumowanie sił w punktach
    for py in y:
        j = 0
        for px in x:
            Z[i, j] = sum_Fo((px, py), qoi, koi, d0) + Fp((px, py), qkr, k0)
            j += 1
        i += 1

    fig = plt.figure(figsize=(10.0, 10.0))
    ax = fig.add_subplot(111)
    ax.set_title('Metoda potencjałów')

    #normalizacja od -1 do 1
    if(norm):
        zmax, zmin = Z.max(), Z.min()
        Z = 2*(Z - zmin)/(zmax - zmin) - 1


    if(flat):
        result = Z.flatten()
        t = np.arange(0, Z.size)
        plt.plot(t, result)
    else:

        plt.imshow(Z, cmap=cm.RdYlGn,
                   origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=20)

        for i in range(0, n_o):
            plt.plot(qoi[i][0], qoi[i][1], "or", color='black')
        plt.plot(qpr[0], qpr[1], "or", color='blue')
        plt.plot(qkr[0], qkr[1], "or", color='blue')

        plt.colorbar(orientation='vertical')

    plt.grid(True)
    plt.show()

main()