import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import random
from scipy.spatial import distance
import math as mt

# generacja przeszkód
def obsticle_creator(n_o):
    qoi = np.zeros([n_o, 2])
    for i in range(0, n_o):
        x1 = random.uniform(-9, 9)
        y1 = random.uniform(-6, 6)
        qoi[i] = [x1, y1]
    return qoi

# liczenie siły przyciągającej
def Fp(qr, qk, kp):
    dist = distance.euclidean(qr, qk)
    return kp * dist

def Fo(qr, qo, ko, d0, limit=10):
    dist = distance.euclidean(qr, qo)
    if dist > d0:
        return 0
    if dist == 0:
        return limit
    ko*=-1
    return ko * (1 / dist - 1 / d0) * (1 / (dist ** 2))

def sum_Fo(qr, tp, koi, d0, limit=10):
    t_Fp = 0
    for i in tp:
        t_Fp += (Fo(qr, i, koi, d0, limit))
    return t_Fp

def sum_Vec(vec1, vec2):
    vec_temp = [vec1[0] + vec2[0], vec1[1] + vec2[1]]
    return vec_temp

def vec_coor(M, alpha):
    return [M * mt.cos(alpha), M * mt.sin(alpha)]

def calc_end_coordinates(qkr, qoi, koi, d0, k0, path, dr=1000):
    end_vec = [0,0]
    for i in qoi:
        if distance.euclidean(i, path[len(path)-1]) < dr:
            alpha = mt.atan((i[1] - path[len(path) - 1][1]) / (i[0] - path[len(path) - 1][0]))  # rad
            alpha1 = alpha * 360 / (2 * mt.pi)

            if alpha1 < 0:

                alpha = 2 * mt.pi + alpha

            Fo_value = Fo(path[len(path) - 1], i, koi, d0)
            t_v = vec_coor(Fo_value, alpha)
            end_vec = sum_Vec(end_vec, t_v)

    v_Fp = Fp(path[len(path)-1], qkr, k0)

    alpha = mt.atan((qkr[1] - path[len(path) - 1][1]) / (qkr[0] - path[len(path) - 1][0]))
    alpha1 = alpha*360/(2*mt.pi)

    if alpha1 < 0:
        alpha = 2 * mt.pi + alpha

    t_Fp = vec_coor(v_Fp, alpha)
    end_vec = sum_Vec(end_vec, t_Fp)

    return end_vec

def calc_alpha(end_vec):
    alpha = mt.atan(end_vec[1]/end_vec[0])
    alpha1 = alpha * 360 / (2 * mt.pi)

    if alpha1 < 0:
        alpha = 2 * mt.pi + alpha

    return alpha

def switcher(alpha):
    alpha1 = alpha*360/(2*mt.pi)
    print("Angle of resultant in degrees", alpha1)
    acc = 0
    if alpha1 is 0:
        return 1, 0
    if alpha1 > 0:
        if alpha1 > 0 - acc and alpha1 < 22.5 + acc:
            return 1, 0
        if alpha1 >= 22.5 - acc and alpha1 < 67.5 + acc:
            return 1, 1
        if alpha1 >= 67.5 - acc and alpha1 < 112.5 + acc:
            return 0, 1
        if alpha1 >= 112.5 - acc and alpha1 < 157.5 + acc:
            return -1, 1
        if alpha1 >= 157.5 - acc and alpha1 < 202.5 + acc:
            return -1, 0
        if alpha1 >= 202.5 - acc and alpha1 < 247.5 + acc:
            return -1, -1
        if alpha1 >= 247.5 - acc and alpha1 < 292.5 + acc:
            return 0, -1
        if alpha1 >= 292.5 - acc and alpha1 < 337.5 + acc:
            return 1, -1
        if alpha1 >= 337.5 - acc and alpha1 < 360 + acc:
            return 1, 0
        print("Error: #01")
        return -2, -2
    print("Error: #02")
    return -3, -3

def path_calc(qpr, qkr, qoi, koi, d0, k0, delta, dr=1000):
    path = list()
    path.append(qpr)  # Dodawnie punktu początkowego do ścieżki

    while True:
        end_vec = calc_end_coordinates(qkr, qoi, koi, d0, k0, path, dr)
        alpha = calc_alpha(end_vec)

        dx, dy = switcher(alpha)

        if dx <= -2 and dy <= -2:
            print("Error: #04")
            return -1

        acc = delta
        path.append([path[len(path) - 1][0] + dx * delta, path[len(path) - 1][1] + dy * delta])

        if path[len(path) - 1][0] > qkr[0] - acc:
            return path

        if path[len(path) - 1][0] > 11:
            print("Error: #03")
            return
def main():
    pathEnbl = True

    n_o = 4
    x_size = 10
    y_size = 10
    delta = .1

    k0 = 1
    koi = 2
    d0 = 20

    # Generacja początkkowego punktu robota
    qpr = [-10, random.randint(-5, 5)]
    # Generacja końcowego punktu robota
    qkr = [10, random.randint(-5, 5)]

    print("Start:", qpr)
    print("Finish:", qkr)

    x = y = np.arange(-10, 10, delta)
    X, Y = np.meshgrid(x, y)

    Z = np.exp(-X ** 0)

    #Generacja przeszkód
    qoi = obsticle_creator(n_o)

    i = 0

    # sumowanie sił w punktach - Macierz Z
    for py in y:
        j = 0
        for px in x:
            fp = Fp((px, py), qkr, k0)
            fo = sum_Fo((px, py), qoi, koi, d0)
            Z[i, j] = fo + fp
            j += 1
        i += 1

    if pathEnbl:
        #Generowanie ścieżki
        path1 = path_calc(qpr, qkr, qoi, koi, d0, k0, delta)


        #Generowanie wektorów do rysowania ścieżki
        if(path1 is -1):
            print("Error: #05")
            return
        else:
            xp = list()
            yp = list()

            for a in path1:
                xp.append(a[0])
                yp.append(a[1])

        path2 = path_calc(qpr, qkr, qoi, koi, d0, k0, delta, 1)

        #Generowanie wektorów do rysowania ścieżki
        if(path2 is -1):
            print("Error: #05")
            return
        else:
            xp1 = list()
            yp1 = list()

            for a in path2:
                xp1.append(a[0])
                yp1.append(a[1])




    fig = plt.figure(figsize=(10.0, 10.0))
    ax = fig.add_subplot(111)
    ax.set_title('Metoda potencjałów')

    plt.imshow(Z, cmap=cm.RdYlGn,
               origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmin = -10)

    for i in range(0, n_o):
        plt.plot(qoi[i][0], qoi[i][1], "or", color='black')
    plt.plot(qpr[0], qpr[1], "or", color='yellow')
    plt.plot(qkr[0], qkr[1], "or", color='yellow')

    if pathEnbl:
        # Rysowanie ścieżki
        line, = plt.plot(xp, yp)
        line1, = plt.plot(xp1, yp1)
        plt.legend((line, line1),('dr=1000', 'dr=5'))

    plt.colorbar(orientation='vertical')

    plt.grid(True)
    plt.show()


main()
