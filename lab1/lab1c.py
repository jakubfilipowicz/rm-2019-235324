import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import random
from scipy.spatial import distance

#generacja przeszkód
def obsticle_creator(n_o):
    qoi = np.zeros([n_o, 2])
    for i in range(0, n_o):
        x1 = random.uniform(-10, 10)
        y1 = random.uniform(-10, 10)
        qoi[i] = [x1, y1]
    return qoi

#liczenie siły przyciągającej
def Fp(qr, qk, kp):
    dist = distance.euclidean(qr, qk)
    return kp*dist

#liczenie siły odpychającej przeszkody
def Fo(qr, qo, ko, d0, limit):
    dist = distance.euclidean(qr, qo)
    if dist > d0:
        return 0
    elif dist == 0:
        return limit
    else:
        return ko * (1 / dist - 1 / d0) * (1 / (dist ** 2))

#sumowanie sił odpychających przeszkód
def sum_Fo(qr, tp, koi, d0, limit=5):
    t_Fp = 0
    for i in tp:
        t_Fp += Fo(qr, i, koi, d0, limit)
    return t_Fp

def calc_with_random_obstacles(k0, koi, d0, fig):
    n_o = 3
    x_size = 10
    y_size = 10
    delta = .1

    qpr = [10, random.randint(-10, 10)]
    qkr = [-10, random.randint(-10, 10)]

    print("Start:", qpr)
    print("Finish:", qkr)

    x = y = np.arange(-10, 10, delta)
    X, Y = np.meshgrid(x, y)

    Z = np.exp(-X ** 0)

    qoi = obsticle_creator(n_o)

    i = 0

    # sumowanie sił w punktach
    for py in y:
        j = 0
        for px in x:
            Z[i, j] = sum_Fo((px, py), qoi, koi, d0) + Fp((px, py), qkr, k0)
            j += 1
        i += 1

    plt.grid(True)

    for i in range(0, n_o):
        plt.plot(qoi[i][0], qoi[i][1], "or", color='black')
    plt.plot(qpr[0], qpr[1], "or", color='blue')
    plt.plot(qkr[0], qkr[1], "or", color='blue')

def main():
    d0i = [1, 5, 10, 20]
    h = 1
    fig = plt.figure(figsize=(10.0, 10.0))
    qpr = [-10, random.randint(-10, 10)]
    qkr = [10, random.randint(-10, 10)]

    for dof in d0i:
        n_o = 3
        x_size = 10
        y_size = 10
        delta = .1

        k0 = 1
        koi = 0.5

        print("Start:", qpr)
        print("Finish:", qkr)

        x = y = np.arange(-10, 10, delta)
        X, Y = np.meshgrid(x, y)

        Z = np.exp(-X ** 0)

        qoi = obsticle_creator(n_o)

        i = 0

        # sumowanie sił w punktach
        for py in y:
            j = 0
            for px in x:
                Z[i, j] = sum_Fo((px, py), qoi, koi, dof) + Fp((px, py), qkr, k0)
                j += 1
            i += 1

        ax = fig.add_subplot(2, 2, h)
        name = "k0 = 1, koi = 0.5, d0 = "
        name += str(dof)
        ax.set_title(name)


        plt.imshow(Z, cmap=cm.RdYlGn,
                   origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=20)

        for i in range(0, n_o):
            plt.plot(qoi[i][0], qoi[i][1], "or", color='black')
        plt.plot(qpr[0], qpr[1], "or", color='blue')
        plt.plot(qkr[0], qkr[1], "or", color='blue')
        plt.grid(True)
        h+=1

    fig1 = plt.figure(figsize=(10.0, 10.0))
    h = 1
    koi = [1, 0.5, 0.25, 2]
    for kof in koi:
        n_o = 3
        x_size = 10
        y_size = 10
        delta = .1

        k0 = 1
        d0 = 20

        print("Start:", qpr)
        print("Finish:", qkr)

        x = y = np.arange(-10, 10, delta)
        X, Y = np.meshgrid(x, y)

        Z = np.exp(-X ** 0)

        qoi = obsticle_creator(n_o)

        i = 0

        # sumowanie sił w punktach
        for py in y:
            j = 0
            for px in x:
                Z[i, j] = sum_Fo((px, py), qoi, kof, d0) + Fp((px, py), qkr, k0)
                j += 1
            i += 1

        ax = fig1.add_subplot(2, 2, h)
        name = "k0 = 1, koi = "
        name += str(kof)
        name += " d0 = 20"
        ax.set_title(name)


        plt.imshow(Z, cmap=cm.RdYlGn,
                   origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=20)

        for i in range(0, n_o):
            plt.plot(qoi[i][0], qoi[i][1], "or", color='black')
        plt.plot(qpr[0], qpr[1], "or", color='blue')
        plt.plot(qkr[0], qkr[1], "or", color='blue')
        plt.grid(True)
        h+=1

    fig2 = plt.figure(figsize=(10.0, 10.0))
    h = 1
    k0i = [1, 2, 3, 4]
    for k0f in k0i:
        n_o = 3
        x_size = 10
        y_size = 10
        delta = .1

        koi = 0.5
        d0 = 20

        print("Start:", qpr)
        print("Finish:", qkr)

        x = y = np.arange(-10, 10, delta)
        X, Y = np.meshgrid(x, y)

        Z = np.exp(-X ** 0)

        qoi = obsticle_creator(n_o)

        i = 0

        # sumowanie sił w punktach
        for py in y:
            j = 0
            for px in x:
                Z[i, j] = sum_Fo((px, py), qoi, koi, d0) + Fp((px, py), qkr, k0f)
                j += 1
            i += 1

        ax = fig2.add_subplot(2, 2, h)
        name = "k0 = "
        name += str(k0f)
        name +=  ", koi = 0.5 d0 = 20"
        ax.set_title(name)

        plt.imshow(Z, cmap=cm.RdYlGn,
                   origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=20)

        for i in range(0, n_o):
            plt.plot(qoi[i][0], qoi[i][1], "or", color='black')
        plt.plot(qpr[0], qpr[1], "or", color='blue')
        plt.plot(qkr[0], qkr[1], "or", color='blue')
        plt.grid(True)
        h += 1

    plt.show()
main()